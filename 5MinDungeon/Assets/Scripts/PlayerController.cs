﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    [Header("Settings")]
    public float timeToStartCharging;
    public float maxChargeTime;
    public float shotDelay = 0;
    float dashModeResetTime = 0;

    [Header("References")]
    public Rigidbody2D rb2d;
    public Camera mainCamera;
    public Transform firePoint;
    public Animator anim;
    public Image dashCooldownImage;
    public Transform enemyIndicator;

    [Header("Prefabs")]
    public GameObject shotPrefab;

    [Header("Accessors")]
    public PlayerStats currentPlayerStats = new PlayerStats();
    public bool isInDashMode = false;

    [Header("Private")]
    Vector2 movementInput;
    GameObject shotObject = null;
    PlasmaShot plasmaShot = null;
    float startTime = 0;
    float currentScale = 0;
    float nextShotTime = 0;
    bool fullCharge = false;
    float nextDashTime = 0;
    int numOfDashes = 1;
    bool isDashing = false;

    bool isShooting = false;
    float shootingTimer = 0;


    bool isDead = false;

    void Update()
    {
        movementInput = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));

        Vector3 inputMousePos = Input.mousePosition;
        //inputMousePos.z = 0;
        Vector3 mouseWorldPosition = mainCamera.ScreenToWorldPoint(inputMousePos);
        mouseWorldPosition.z = 0;

        transform.up = mouseWorldPosition - transform.position;


        if (Time.time > nextShotTime)
        {
            ProccessFire();
        }

        if (!isDead && LevelManager.instance.time <= 0)
        {
            PlayDeath();
        }

        if (rb2d.velocity.sqrMagnitude > 2)
        {
            anim.SetBool("isWalking", true);
        }
        else
        {
            anim.SetBool("isWalking", false);
        }

        if (Time.time > nextDashTime)
        {
            //            print("hi");
            numOfDashes = Mathf.Clamp(++numOfDashes, 0, currentPlayerStats.numberOfDashes);
            nextDashTime = Time.time + currentPlayerStats.dashesCooldownTime;
        }

        if (Input.GetKeyDown(KeyCode.Space) && numOfDashes > 0)
        {
            --numOfDashes;
            isDashing = true;
            if (numOfDashes == currentPlayerStats.numberOfDashes - 1)
            {
                nextDashTime = Time.time + currentPlayerStats.dashesCooldownTime;
            }
            AudioManger.instance.PlaySound(2, Random.Range(.5f, .7f), Random.Range(.7f, 1.3f));
        }

        if (Time.time > dashModeResetTime)
        {
            isInDashMode = false;
        }

        //        print(((Time.time - nextDashTime) / (float)currentPlayerStats.dashesCooldownTime));

        if (numOfDashes == 0)
        {
            dashCooldownImage.fillAmount = 1 - ((nextDashTime - Time.time) / (float)currentPlayerStats.dashesCooldownTime);
        }
        else
        {
            dashCooldownImage.fillAmount = 1;
        }
        //        print(numOfDashes +"   " + Time.time/nextDashTime);

        if(isShooting)
        {
            LevelManager.instance.enemyIndicator.gameObject.SetActive(false);
        }
        else
        {
            LevelManager.instance.enemyIndicator.gameObject.SetActive(true);
        }


        if (Time.time > shootingTimer)
        {
            isShooting = false;
        }
    }

    void PlayDeath()
    {
        isDead = true;
        nextShotTime = Time.time + 10;
        GameObject destroyParticleSystem = transform.GetChild(1).gameObject;
        destroyParticleSystem.SetActive(true);
        GetComponent<SpriteRenderer>().enabled = false;
        GetComponent<EdgeCollider2D>().enabled = false;
        GetComponent<Rigidbody2D>().isKinematic = true;
        destroyParticleSystem.GetComponent<ParticleSystem>().Play();
        AudioManger.instance.PlaySound(3, Random.Range(.2f, .4f), Random.Range(.7f, 1.3f));
        Destroy(gameObject, 1f);
    }

    void ProccessFire()
    {
        if (Input.GetMouseButtonDown(0))
        {
            shotObject = Instantiate(shotPrefab, firePoint.position, Quaternion.identity).gameObject;
            plasmaShot = shotObject.GetComponent<PlasmaShot>();
            plasmaShot.StopChargeParticle();
            currentScale = plasmaShot.transform.localScale.x;
            startTime = Time.time;
            plasmaShot.SetLayer(LayerMask.NameToLayer("PlayerBullet"));
            shotObject.GetComponent<Rigidbody2D>().isKinematic = true;
            isShooting = true;
            shootingTimer = Time.time + 2f;
        }
        else if (Input.GetMouseButton(0) && shotObject != null)
        {
            shotObject.transform.position = firePoint.position;
            if (Time.time > startTime + timeToStartCharging && Time.time < startTime + maxChargeTime)
            {
                plasmaShot.PlayChargeParticle();
                shotObject.transform.localScale = shotObject.transform.localScale * 1.01f;
                currentScale *= 1.01f;
                AudioManger.instance.PlaySound(0, Random.Range(.5f, .7f), Random.Range(.7f, 1.3f));
            }
            else if (Time.time < startTime + maxChargeTime)
            {
                fullCharge = true;
                AudioManger.instance.StopCharge();
            }
            else
            {
                plasmaShot.StopChargeParticle();
                AudioManger.instance.StopCharge();
            }
            isShooting = true;
            shootingTimer = Time.time + 2f;
        }
        else if (Input.GetMouseButtonUp(0) && shotObject != null)
        {
            AudioManger.instance.StopCharge();

            shotObject.GetComponent<Rigidbody2D>().isKinematic = false;
            shotObject.transform.parent = null;
            //shotObject.transform.position += transform.up * shotObject.transform.localScale.x/4; 
            shotObject.GetComponent<Rigidbody2D>().AddForce(transform.up * (5 * currentPlayerStats.shotSpeed) / (currentScale));
            plasmaShot.Shoot((currentScale + .75f) * currentPlayerStats.shotDamage + (currentScale - .25f) * currentPlayerStats.shotDamage, fullCharge);
            nextShotTime = Time.time + shotDelay;
            shotObject = null;
            isShooting = true;
            shootingTimer = Time.time + 2f;

            AudioManger.instance.PlaySound(5, Random.Range(.2f, .4f), Random.Range(.7f, 1.3f));
        }
    }

    void FixedUpdate()
    {
        if (movementInput.sqrMagnitude < 0.01f)
        {
            rb2d.drag = 8;
        }
        else
        {
            rb2d.drag = 2;
        }

        if (!isDashing && rb2d.velocity.magnitude <= currentPlayerStats.maxSpeed)
        {
            rb2d.AddForce((movementInput.normalized) * currentPlayerStats.acceleration);
            rb2d.velocity = Vector2.ClampMagnitude(rb2d.velocity, currentPlayerStats.maxSpeed);
        }
        else if (isDashing)
        {
            rb2d.AddForce((movementInput.normalized) * currentPlayerStats.dashSpeed * 50);
            isDashing = false;
            isInDashMode = true;
            dashModeResetTime = Time.time + .75f;
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (isInDashMode && collision.gameObject.layer == LayerMask.NameToLayer("Enemy"))
        {
            collision.gameObject.GetComponent<EnemyController>().health -= currentPlayerStats.dashDamage;
            StartCoroutine(CameraManager.instance.CameraShake(.6f));
            isInDashMode = false;

            AudioManger.instance.PlaySound(1, Random.Range(.2f, .4f), Random.Range(.7f, 1.3f));
        }
    }
}
