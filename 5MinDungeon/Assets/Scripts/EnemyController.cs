﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class EnemyController : MonoBehaviour
{
    //[Header("Settings")]

    [Header("References")]
    public Transform healthBar;
    public Transform firePoint;
    public Rigidbody2D rb2d;
    public PlayerController player;
    public float shotDelay = .5f;
    public GameObject destroyParticleSystem;
    public SpriteRenderer spriteRenderer;
    public Animator anim;

    [Header("Prefabs")]
    public GameObject plasmaShotPrefab;

    [Header("Accessor")]
    public float health;

    [Header("Private")]
    PlayerStats enemyStats = new PlayerStats();
    List<Vector2> velocities = new List<Vector2>();
    float nextShotTime = 0;


    void Awake()
    {
        rb2d.drag = 2;
        enemyStats.shotDamage = Random.Range(4,10);
        enemyStats.shotSpeed = Random.Range(35, 50);
        enemyStats.acceleration = Random.Range(6, 14);
        enemyStats.maxSpeed = Random.Range(50, 70);
        health = Random.Range(18, 37);

        velocities.Add(new Vector2(Random.Range(-5, 5), Random.Range(-5, 5)));
        velocities.Add(new Vector2(Random.Range(-5, 5), Random.Range(-5, 5)));
        velocities.Add(new Vector2(Random.Range(-5, 5), Random.Range(-5, 5)));
        velocities.Add(new Vector2(Random.Range(-5, 5), Random.Range(-5, 5)));
        velocities.Add(new Vector2(Random.Range(-5, 5), Random.Range(-5, 5)));
        velocities.Add(new Vector2(Random.Range(-5, 5), Random.Range(-5, 5)));
        velocities.Add(new Vector2(Random.Range(-5, 5), Random.Range(-5, 5)));
        velocities.Add(new Vector2(Random.Range(-5, 5), Random.Range(-5, 5)));
        velocities.Add(new Vector2(Random.Range(-5, 5), Random.Range(-5, 5)));
        velocities.Add(new Vector2(Random.Range(-5, 5), Random.Range(-5, 5)));
    }

    void Update()
    {
        if (player != null)
        {
            enemyStats.acceleration = Random.Range(6, 14);
            if (health <= 0)
            {
                PlayDeath();
            }

            Vector2 aimDirection = player.transform.position - transform.position;
            //aimDirection += new Vector2(Random.Range(0, 10), Random.Range(0, 10));
            transform.up = aimDirection;

            if (Time.time > nextShotTime && Vector2.Distance(player.transform.position, transform.position)<7)
            {
                ProccessFire();
            } 
        }
        healthBar.position = transform.position + new Vector3(0, 0.478f);
        healthBar.localScale = new Vector2(health / 5f, healthBar.localScale.y);

        if(rb2d.velocity.sqrMagnitude > 2)
        {
            anim.SetBool("isWalking", true);
        }
        else
        {
            anim.SetBool("isWalking", false);
        }
    }

    void ProccessFire()
    {
        GameObject shotObject = Instantiate(plasmaShotPrefab, firePoint.position, Quaternion.identity).gameObject;
        PlasmaShot plasmaShot = shotObject.GetComponent<PlasmaShot>();
        plasmaShot.SetLayer(LayerMask.NameToLayer("EnemyBullet"));
        shotObject.transform.parent = null;

        shotObject.GetComponent<Rigidbody2D>().AddForce(transform.up * (5 * enemyStats.shotSpeed));
        plasmaShot.Shoot(enemyStats.shotDamage);
        nextShotTime = Time.time + shotDelay;
        shotObject = null;

        AudioManger.instance.PlaySound(5, Random.Range(.1f, .3f), Random.Range(.7f, 1.3f));

    }

    void PlayDeath()
    {
        nextShotTime = Time.time + 10;
        health = 1000;
        LevelManager.instance.time += 6;
        destroyParticleSystem.SetActive(true);
        spriteRenderer.enabled = false;
        healthBar.GetComponent<SpriteRenderer>().enabled = false;
        GetComponent<BoxCollider2D>().enabled = false;
        GetComponent<Rigidbody2D>().isKinematic = true;
        destroyParticleSystem.GetComponent<ParticleSystem>().Play();
        AudioManger.instance.PlaySound(3, Random.Range(.2f, .4f), Random.Range(.7f, 1.3f));
        Destroy(transform.parent.gameObject, 1f);
    }

    void FixedUpdate()
    {
        if (true)
        {
            for (int i = 0; i < 2; i++)
            {
                if (player != null) velocities[i] = player.transform.position - transform.position;
            }
        }
        Vector3 sum = Vector3.zero;
        foreach (Vector3 vel in velocities)
        {
            sum = sum + vel;
        }
        sum = sum / velocities.Count;
        velocities.RemoveAt(0);
        velocities.Add(new Vector2(Random.Range(-5, 5), Random.Range(-5, 5)));

        rb2d.AddForce((sum.normalized) * enemyStats.acceleration);
        rb2d.velocity = Vector2.ClampMagnitude(rb2d.velocity, enemyStats.maxSpeed);
    }
}
