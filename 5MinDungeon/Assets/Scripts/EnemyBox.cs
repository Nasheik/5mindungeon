﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBox : MonoBehaviour
{
    //[Header("Settings")]

    [Header("References")]
    public List<GameObject> enemyControllers = new List<GameObject>();

    //[Header("Prefabs")]

    //[Header("Accessors")]

    //[Header("Private")]

    private void Awake()
    {
        foreach (GameObject enemy in enemyControllers)
        {
            enemy.gameObject.SetActive(false);
        }
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            foreach (GameObject enemy in enemyControllers)
            {
                if(enemy!=null) enemy.gameObject.SetActive(true);

            }
        }
    }
}
