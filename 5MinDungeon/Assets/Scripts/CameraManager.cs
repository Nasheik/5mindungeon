﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    //[Header("Settings")]

    [Header("References")]
    public Transform player;
    public Transform cameraTransform;
    public Texture2D cursorTex;


    public static CameraManager instance;
    //[Header("Prefabs")]

    //[Header("Private")]

    void Awake()
    {
        if (instance == null) instance = this;
        else Destroy(this);
    }

    void Start()
    {
        Cursor.SetCursor(cursorTex, new Vector2(cursorTex.width / 2, cursorTex.height / 2), CursorMode.Auto);
    }

    void FixedUpdate()
    {

        if (player != null)
        {
            Vector3 mouseVec = Camera.main.ScreenToWorldPoint(Input.mousePosition) - player.position;
            mouseVec.z = 0;
            mouseVec = Vector3.ClampMagnitude(mouseVec, 3f);
            //            print(mouseVec);
            //print(mouseVec.sqrMagnitude);
            transform.position = player.position + mouseVec / 3; // + (Vector3)mouseVec; 
        }
    }

    bool isRunning = false;
    public IEnumerator CameraShake(float shakeMag)
    {
        if (!isRunning)
        {
            isRunning = true;
            float duration = .02f;
            float currentTime = 0;
            Vector3 randomPos = cameraTransform.localPosition + new Vector3(Random.Range(-1, 1), Random.Range(-1, 1));
            randomPos.z = 0;
            randomPos = randomPos.normalized;
            randomPos *= shakeMag;
            randomPos.z = -10;
            while (currentTime < duration)
            {
                cameraTransform.localPosition = Vector3.Lerp(cameraTransform.localPosition, randomPos, currentTime / duration);
                currentTime += Time.deltaTime;
                yield return null;
            }
            cameraTransform.localPosition = randomPos;


            duration = .04f;
            currentTime = 0;
            randomPos = -randomPos;
            randomPos.z = -10;
            while (currentTime < duration)
            {
                cameraTransform.localPosition = Vector3.Lerp(cameraTransform.localPosition, randomPos, currentTime / duration);
                currentTime += Time.deltaTime;
                yield return null;
            }
            cameraTransform.localPosition = randomPos;


            duration = .02f;
            currentTime = 0;
            Vector3 regPos = new Vector3(0, 0, -10);
            while (currentTime < duration)
            {
                cameraTransform.localPosition = Vector3.Lerp(cameraTransform.localPosition, regPos, currentTime / duration);
                currentTime += Time.deltaTime;
                yield return null;
            }
            cameraTransform.localPosition = regPos;
            isRunning = false;
        }
    }
}
