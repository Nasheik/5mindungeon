﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour
{
    //[Header("Settings")]

    [Header("References")]
    public Transform player;
    public Transform enemyIndicator;
    public Text timerText;
    public Text enemyText;
    public List<Transform> enemyBoxes = new List<Transform>(); 

    //[Header("Prefabs")]

    [Header("Accessors")]
    public static LevelManager instance;
    public float time = 300;

    //[Header("Private")]
    bool timeStarted = false;
    bool isWon = false;

    void Awake()
    {
        if (instance == null) instance = this;
        else Destroy(this);
    }

    void Start()
    {

    }

    void Update()
    {
        if (player != null)
        {
            enemyIndicator.position = player.position;

            if (GetEnemyCount() > 0)
            {
                enemyIndicator.up = GetActiveEnemyPosition() - player.position;
            }
            else
            {
                enemyIndicator.up = (transform.position + (Vector3)GetComponent<BoxCollider2D>().offset) - player.position;
            }
        }
        else
        {
            enemyIndicator.gameObject.SetActive(false);
        }

        if (timeStarted)
        {
            time = Mathf.Clamp(time - Time.deltaTime, -1, 500);
        }
        timerText.text = (((int)time) + 1).ToString();

        if (GetEnemyCount() > 0)
        {
            enemyText.text = GetEnemyCount() + " Left";
        }
        else
        {
            enemyText.text = "Run Back";
            enemyText.color = Color.green;
        }

        if(isWon)
        {
            enemyText.text = "You Won \\o/";
            enemyText.color = Color.green;
        }
    }

    public int GetEnemyCount()
    {
        int sum = 0;
        foreach (Transform transform in enemyBoxes)
        {
            sum += transform.childCount;
        }
        return sum;
    }

    public Vector3 GetActiveEnemyPosition()
    {
        foreach (Transform boxTransform in enemyBoxes)
        {
            if (boxTransform.childCount>0)
            {
                if (boxTransform.GetChild(0) != null && boxTransform.GetChild(0).gameObject.activeSelf == false)
                {
                    return boxTransform.GetChild(0).position;
                } 
            }
        }
        return Vector3.zero;
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            if (GetEnemyCount() > 0)
            {
                timeStarted = true;
            }
            else
            {
                timeStarted = false;
                isWon = true;
            }
        }
    }


    public void ProcessShotDmg(bool buy)
    {
        if (buy)
        {
            player.GetComponent<PlayerController>().currentPlayerStats.shotDamage = 12;
            time = time - 60;
        }
        else
        {
            player.GetComponent<PlayerController>().currentPlayerStats.shotDamage = 6;
            time = time + 60;
        }
    }

    public void ProcessShotSpeed(bool buy)
    {
        if (buy)
        {
            player.GetComponent<PlayerController>().currentPlayerStats.shotSpeed = 60;
            time = time - 60;
        }
        else
        {
            player.GetComponent<PlayerController>().currentPlayerStats.shotSpeed = 30;
            time = time + 60;
        }
    }

    public void ProcessMoveSpeed(bool buy)
    {
        if (buy)
        {
            player.GetComponent<PlayerController>().currentPlayerStats.acceleration = 20;
            player.GetComponent<PlayerController>().currentPlayerStats.maxSpeed = 200;
            time = time - 60;
        }
        else
        {
            player.GetComponent<PlayerController>().currentPlayerStats.acceleration = 10;
            player.GetComponent<PlayerController>().currentPlayerStats.maxSpeed = 100;
            time = time + 60;
        }
    }

    public void ProcessDashes(bool buy)
    {
        if (buy)
        {
            player.GetComponent<PlayerController>().currentPlayerStats.numberOfDashes = 2;
            time = time - 30;
        }
        else
        {
            player.GetComponent<PlayerController>().currentPlayerStats.numberOfDashes = 1;
            time = time + 30;
        }
    }

    public void ProcessDashCooldown(bool buy)
    {
        if (buy)
        {
            player.GetComponent<PlayerController>().currentPlayerStats.dashesCooldownTime = 2;
            time = time - 60;
        }
        else
        {
            player.GetComponent<PlayerController>().currentPlayerStats.dashesCooldownTime = 3;
            time = time + 60;
        }
    }

    public void ProcessDashDmg(bool buy)
    {
        if (buy)
        {
            player.GetComponent<PlayerController>().currentPlayerStats.dashDamage = 30;
            time = time - 60;
        }
        else
        {
            player.GetComponent<PlayerController>().currentPlayerStats.dashDamage = 17;
            time = time + 60;
        }
    }
}
