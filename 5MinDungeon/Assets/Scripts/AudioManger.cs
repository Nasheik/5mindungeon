﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManger : MonoBehaviour
{
    //[Header("Settings")]

    [Header("References")]
    public List<AudioClip> audioClips = new List<AudioClip>();
    //public AudioClip charge;//0
    //public AudioClip collide;//1
    //public AudioClip dash;//2
    //public AudioClip destroy;//3
    //public AudioClip shot;//4
    //laser shoot // 5

    [Header("Prefabs")]
    public GameObject soundPrefab;

    [Header("Accessors")]
    public static AudioManger instance;

    //[Header("Private")]


    void Awake()
    {
        if (instance == null) instance = this;
        else Destroy(this);
    }

    GameObject currentCharge;
    public void StopCharge()
    {
        if(currentCharge!=null)
        {
            currentCharge.GetComponent<AudioSource>().Stop();
            Destroy(currentCharge);
        }
        currentCharge = null;
    }

    public void PlaySound(int soundIndex, float volume, float pitch)
    {
        GameObject soundObject = Instantiate(soundPrefab, transform).gameObject;
        AudioSource audioSource = soundObject.GetComponent<AudioSource>();

        if (soundIndex == 0)
        {
            if (currentCharge == null)
            {
                currentCharge = soundObject;
            }
            else
            {
                return;
            }
        }

        audioSource.clip = audioClips[soundIndex];
        audioSource.volume = volume;
        //audioSource.volume = Random.Range(.65f, 1);
        audioSource.pitch = pitch;
        //audioSource.pitch = Random.Range(-.4f, .4f);

        audioSource.Play();

        Destroy(soundObject, audioClips[soundIndex].length);
    }
    
}
