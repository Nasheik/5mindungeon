﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats
{
    public int shotDamage = 6;
    public int fullyChargedShotDamage = 30;//
    public int accuracy = 50;//
    public int acceleration = 10;
    public int maxSpeed = 100;
    public int shotSpeed = 30;//
    public int numberOfDashes = 1;//
    public float dashesCooldownTime = 3f;//
    public int dashSpeed = 10;
    public int dashDamage = 10;
}
