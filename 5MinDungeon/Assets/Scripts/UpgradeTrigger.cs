﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpgradeTrigger : MonoBehaviour
{
    [Header("Settings")]
    public bool isShotDmg;
    public bool isShotSpeed;
    public bool isMoveSpeed;
    public bool isDashes;
    public bool isDashCD;
    public bool isDashDmg;

    bool isOn;
    //[Header("References")]

    //[Header("Prefabs")]

    //[Header("Accessors")]

    //[Header("Private")]

    //float triggerTimer = 0;
    //bool takeTriggerInput = true;

    //void Update()
    //{
    //    if(Time.time)
    //}

    void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.layer == LayerMask.NameToLayer("PlayerBullet"))
        {
            isOn = !isOn;
            if (isOn) GetComponent<SpriteRenderer>().color = Color.green;
            else GetComponent<SpriteRenderer>().color = Color.red;

            if (isShotDmg)
            {
                LevelManager.instance.ProcessShotDmg(isOn);
            }
            if (isShotSpeed)
            {
                LevelManager.instance.ProcessShotSpeed(isOn);
            }
            if (isMoveSpeed)
            {
                LevelManager.instance.ProcessMoveSpeed(isOn);
            }
            if (isDashes)
            {
                LevelManager.instance.ProcessDashes(isOn);
            }
            if (isDashCD)
            {
                LevelManager.instance.ProcessDashCooldown(isOn);
            }
            if (isDashDmg)
            {
                LevelManager.instance.ProcessDashDmg(isOn);
            }
        }
    }
}
