﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlasmaShot : MonoBehaviour
{
    [Header("Settings")]
    public int maxShotHitCount = 3;

    [Header("References")]
    public GameObject chargeParticleSystem;
    public GameObject destroyParticleSystem;
    public CircleCollider2D regCollider;
    public CircleCollider2D triggerCollider;

    //[Header("Prefabs")]

    [Header("Accessors")]
    public float damage;

    [Header("Private")]
    float birthTime = -1;
    int shotHitCount = 0;
    bool fullCharge = false;


    public void SetLayer(int layer)
    {
        gameObject.layer = layer;
    }

    void Awake()
    {
        destroyParticleSystem.GetComponent<ParticleSystem>().Stop();
        shotHitCount = maxShotHitCount;
        regCollider.enabled = false;
        triggerCollider.enabled = true;
    }

    void Update()
    {
        //chargeParticleSystem.transform.up = Vector2.up;
        if (birthTime > 0 && Time.time - birthTime > 3f)
        {
            PlayDestroyParticle();
        }
        if(shotHitCount<=0)
        {
            PlayDestroyParticle();
        }
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        //PlayDestroyParticle();
        //print((collision.gameObject.name));
        if (collision.gameObject.layer == LayerMask.NameToLayer("Wall"))
        {
            shotHitCount = 0;
            AudioManger.instance.PlaySound(4, Random.Range(.5f, .7f), Random.Range(.7f,1.3f));
        }
        else if(collision.gameObject.layer == LayerMask.NameToLayer("PlayerBullet"))
        {
            shotHitCount--;
            AudioManger.instance.PlaySound(4, Random.Range(.5f, .7f), Random.Range(.7f, 1.3f));
        }
        else if(collision.gameObject.layer == LayerMask.NameToLayer("EnemyBullet"))
        {
            shotHitCount--;
            AudioManger.instance.PlaySound(4, Random.Range(.5f, .7f), Random.Range(.7f, 1.3f));
        }
        else if (collision.gameObject.layer == LayerMask.NameToLayer("Enemy"))
        {
            collision.gameObject.GetComponent<EnemyController>().health -= damage;
            if (fullCharge)
            {
                StartCoroutine(CameraManager.instance.CameraShake(.4f));
            }
            else
            {
                StartCoroutine(CameraManager.instance.CameraShake(.2f));
            }
            AudioManger.instance.PlaySound(4, Random.Range(.5f, .7f), Random.Range(.7f, 1.3f));
            //print(damage);
            //LevelManager.instance.time += 2;
            shotHitCount = 0;
        }
        else if (collision.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            if (!LevelManager.instance.player.GetComponent<PlayerController>().isInDashMode)
            {
                StartCoroutine(CameraManager.instance.CameraShake(.2f));
                LevelManager.instance.time -= 10;
                AudioManger.instance.PlaySound(4, Random.Range(.5f, .7f), Random.Range(.7f, 1.3f));
            }
            shotHitCount = 0;
        }
        if (shotHitCount <= 0)
        {
            PlayDestroyParticle();
        }
    }

    //void OnTriggerEnter2D(Collider2D collision)
    //{
    //    print((collision.gameObject.name));
    //    if (collision.gameObject.layer == LayerMask.NameToLayer("Wall"))
    //    {
    //        shotHitCount = 0;
    //    }
    //    //TODO If enemy
    //}

    public void PlayChargeParticle()
    {
        chargeParticleSystem.SetActive(true);
        chargeParticleSystem.GetComponent<ParticleSystem>().Play();
    }

    public void StopChargeParticle()
    {
        chargeParticleSystem.SetActive(false);
    }

    public void Shoot(float damageArg, bool fullChargeArg = false)
    {
        damage = damageArg;
        StopChargeParticle();
        regCollider.enabled = true;
        triggerCollider.enabled = false;
        birthTime = Time.time;

        if (fullChargeArg) { shotHitCount = 2; fullCharge = true; }
        else shotHitCount = 1;
    }

    public void PlayDestroyParticle()
    {
        StopChargeParticle();
        destroyParticleSystem.SetActive(true);
        GetComponent<SpriteRenderer>().enabled = false;
        GetComponent<CircleCollider2D>().enabled = false;
        GetComponent<Rigidbody2D>().isKinematic = true;
        destroyParticleSystem.GetComponent<ParticleSystem>().Play();
        Destroy(gameObject, 1f);
    }
}
